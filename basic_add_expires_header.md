# 3.添加Expires头

####条件GET请求
如果浏览器在其缓存中保留了资源的一个副本，但并不确定它是否有效，就会生成一个条件GET请求。如果确认缓存的副本依然有效，浏览器就可以使用缓存中的副本，这会得到更小的响应和更快的用户体检。

request: ```If-Modified-Since: Wed, 22 Feb 2006 04:15:22 GMT```

response: ```Last-Modified: Wed, 22 Feb 2006 04:15:22 GMT```

缺点：条件GET请求和304响应有助于让页面加载更快，但仍需要客户端和服务器之间进行一次往返确认。

####Expire
Expire头通过确定指明浏览器是否可以使用资源的缓存副本，来消除条件GET请求。

* 通过使用一个长久的Expires头，可以使资源被缓存，在后续的页面浏览中避免不必要的HTTP请求。

* 长久的Expires头可用于图片，脚本，样式表和Flash。

response: ```Expires: Mon, 15 Apr 2024 20:00:00 GMT```

####缺点
因为Expires头使用一个特定的时间，它要求服务器和客户端的时间严格同步。另外，过期时间需要经常检查，如果这一天来了，还需要在服务器配置中提供一个新的时间。

####Cache-Control
HTTP1.1引入Cache-Control来克服Expires头的限制。

可以使用max-age指令指定资源被缓存多久，以秒为单位，如果从资源被请求开始过去的秒数少于max-age，浏览器就使用缓存的版本。

response: ```Cache-Control: max-age=315360000```

**Cache-Control的优先级要大于Expires**

####如何不使用缓存
为资源的名字采取变量，如果加hash后缀，或时间戳，版本号。