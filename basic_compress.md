# 4.压缩组件
request: ```Accept-Encoding: gzip, deflate```

response:```Content-Encoding:gzip```

####代理缓存
如果第一次请求来自不支持gizp浏览器，那么代理服务器会将请求转发到Web服务之后，将得到的返回内容，以未压缩格式存储；第二次请求同样URL，来自支持gizp浏览器，这时代理服务器会直接返回缓存中的未压缩内容。

如何解决？

1.在Web服务器的响应头里家Vary头：

response: ```Vary: Accept-Encoding```

Vary可以使代理服务器缓存响应的多个版本，为Accept-Encoding请求头的每一个值缓存一份。

如上面的例子，会分别缓存Accept-Encoding为gzip时的压缩内容和没有指定Accept-Encoding时的非压缩内容。

2.禁止代理缓存

```Cache-Control: private```
