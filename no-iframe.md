#少用iframe

使用iframe的缺点如下：

#### 1. 它是开销最高的DOM元素

#### 2. 不利于SEO

搜索引擎的检索程序无法解读iframe中的src

#### 3. 阻塞onload事件

iframe不加载完毕，就不会触发父窗口的onload事件。

#### 4. 影响页面资源并行加载

iframe和主页面共享连接池，而浏览器对相同域的连接有限制，所以会影响页面资源的并行加载。

**为了解决问题3和问题4，可以动态设置iframe中的src属性**，代码如下：

```
<iframe id="iframe1" src=""></iframe>
<script>
document.getElementById('iframe1').src = "www.api.a.com";
</script>
```