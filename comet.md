#Comet技术

“服务器推”是一种很早就存在的技术，以前在实现上主要是通过客户端的套接口，或是服务器端的远程调用。最近几年，因为 AJAX 技术的普及，以及把 IFrame 嵌在“htmlfile“的 ActiveX 组件中可以解决 IE 的加载显示问题，一些受欢迎的应用如 meebo，gmail+gtalk 在实现中使用了这些新技术；同时“服务器推”在现实应用中确实存在很多需求。

概念：**Alex Russell（Dojo Toolkit 的项目 Lead）称这种基于 HTTP 长连接、无须在浏览器端安装插件的“服务器推”技术为“Comet”。**

实现方式有如下方式：

#### 1. 基于 AJAX 的简单轮询方式

最简单的是前台轮询，每隔一段时间去请求后台，以获取最新状态，这种方式最容易实现，但效果也最差，频繁盲目的调用后台，带来不必要的开销，且实时性无法保障，后台出现更新，前端需要在下一次轮询时才知道。

```
setTimeout(function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function(){
        if (xhr.readystate == 4) {
            // 处理服务端返回结果
        }
    };
    xhr.send(null);
}, 2000)
```

**不支持跨域**

#### 2. 基于 AJAX 的长轮询（long-polling）方式
为了解决简单轮询的弊端，进化出长轮询技术，轮询请求会在后台阻塞，相当于保持一个长连接，直到后台出现更新或者超时才返回，这样就可以保证更新的及时性，前端得到请求后，重新建立轮询连接，等待后台的更新通知。

![](/assets/long-polling.jpg)

```
function longPoll(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function(){
        if (xhr.readystate == 4) {
            callback(xhr.responseText);
            // 发送另一个请求，重新连接服务器
            xhr.open('GET', url, true);
            xhr.send(null);
        }
    };
    xhr.send(null);

}
```
**不支持跨域**

#### 3. 永久帧

打开一个隐藏的iframe，请求一个基于HTTP1.1块编码的文档。块编码是为增量读取超大文档而设计的，所以可以将其看做一个不断增加内容的文档。

```
function foreverFrame(url, callback) {
    var iframe = body.appendChild(document.createElement('iframe'));
    iframe.style.display = 'none';
    iframe.src = url + '?callback=parent.foreverFrame.callback';
    this.callback = callback;
}
```

服务器会发送一系列信息到iframe中，如下：
```
<script type="text/javascript">
    parent.foreverFrame.callback('the first message');
</script>
```
**支持跨域**

#### 4. XHR流

XHR流允许服务器连续的发送消息，无需每次响应后再去建立一个新请求。

![](/assets/iframe-polling.jpg)

```
function xhrStreaming(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    var lastSize;
    xhr.onreadystatechange = function(){
        var newTextReceived;
        if (xhr.readystate == 2) {
            // 获取最新的响应正文
            newTextReceived = xhr.responseText.substring(lastSize);
            lastSize = xhr.responseText.length;
            callback(newTextReceived);
        }
        if (xhr.readystate == 4) {
            // 如果响应结束，马上创建一个新的请求
            xhrStreaming(url, callback);
        }
    };
    xhr.send(null);
}
```
**不支持跨域**

#### 5. 回调轮询（callback polling）/ JSONP轮询

为每个请求插入一个script，从而实现跨域轮询，而不用依赖于XHR。

```
function callbackPolling(url, callback){
    var script =  document.createElement('script');
    script.type = 'text/javascript';
    script.src = url + 'callback=callbackPolling.callback';
    callbackPolling.callback = function(){
         // 发送一个新的请求等待服务器下次发送的消息 
         callbackPolling(url, callback);
         // 调用回调函数
         callback();
    }
    // 添加这个元素，开始执行脚本
    document.getElementByTagName('head')[0].appendChild(script);
}
```
**支持跨域**

