#localStorage读取性能

本地存储历史

![](/assets/localstorage.jpg)

localStorage浏览器支持情况

![](/assets/localstorage-support.jpg)

####问题：

1. localStorage 的实现方式是在硬盘中保存一个文件，当需要读取数据时，便要把文件读取到内存中，**这个过程是同步的**，这也就意味着浏览器什么也不能做，直到文件读取完毕。
2. 当第一次使用 localStorage 读取数据时，浏览器从效率角度考虑需要把**文件读取到内存中缓存起来**，这个读取的过程便涉及到了文件 IO 操作。
IO 操作是个容易受到众多不稳定因素影响的操作，例如 Windows 的索引服务，杀毒软件的操作等，这就导致 IO 操作的时间可能从 0 毫秒到几秒钟不等，浏览器也会因此失去响应，这对于一个网站来说是致命的缺点。

####优化：

1. 数据的大小不会对 API 调用产生影响，因此应该尽量向一个条目中存储多的内容，减少键值。
2. 使用 localStorage 的 getItem() 而不是直接通过 key 来读取数据的效率会更好。

```
var a1 = localStorage["a"];//不推荐：获取a的值
var a2 = localStorage.a;//不推荐：获取a的值
var a3 = localStorage.getItem("a");//推荐：获取a的值
```
