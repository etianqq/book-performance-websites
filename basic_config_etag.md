# 13.配置ETag

实体标签(Entity Tag, ETag)是Web服务器和浏览器用于确认缓存组件有效性的一种机制。

ETag在HTTP1.1中引入，是唯一标识一个资源的一个特定版本的字符串。

**ETag的加入微验证实体提供了比最新修改日期更为灵活的机制**。例如，如果实体根据User-Agent或Accept-Language头而改变，实体的状态可以反映在ETag中。

request: ```If-None-Match:"eot99-f4ab-6kh"```

response:```ETag:"eot99-f4ab-6kh"; Cache-Control:no-cache```

注意：```If-None-Match```的权重要大于条件GET中的```If-Modified-Since```

![](etag.png)
####和Cache-Control一起使用
Cache-Control: ```max-age/private/public/no-cache/no-store```

![](http-cache-decision-tree.png)

####带来的问题
如果使用服务器集群，那么，服务器们不可以共享相同的ETag。

如果使用代理服务器，代理后面用户缓存的ETag经常会和代理缓存的ETag不匹配，导致不必要的请求被发送到原始服务器。


更多HTTP Caching的内容，参考：[https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching)


