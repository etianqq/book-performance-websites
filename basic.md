# 基础篇

和浏览器缓存相关的章节：
* 3.添加Expires头
* 13.配置ETag


####绪论：HTTP

#####1.压缩
request: ```Accept-Encoding:gzip,deflate```

response:```Content-Encoding:gzip```

#####2.条件GET
request: ```If-Modified-Since: Wed, 22 Feb 2006 04:15:22 GMT```

response: ```Last-Modified: Wed, 22 Feb 2006 04:15:22 GMT```

#####3.Expires
response: ```Expires: Mon, 15 Apr 2024 20:00:00 GMT```

#####4.Keep-Alive
HTTP早期实现中，每个HTTP请求都要打开一个socket连接，这样效率很低，因为一个页面中很多HTTP请求都指向同一个服务器。

持久连接(persistent connection)使浏览器可以在一个单独的连接上进行多个请求。浏览器和服务器使用Connection头来指出对Keep-Alive的支持。

request:```Connection:keep-alive```

response:```Connection:keep-alive```

浏览器和服务器可以通过```Connection:close```来关闭连接。

