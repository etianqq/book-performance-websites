# 1.减少HTTP请求

1. 使用CSS Sprites，把多个图片合并到一个单独的图片中。
2. 使用```data:URL```展示图片。缺点：
    1. 此方案不适合mobile应用。
    2. IE7以下不支持
    3. 跨页面不会被缓存
3. 合并脚本和样式表
4. **Multipart XHR**
   
   运行客户端用一个HTTP请求就可以从服务端传递多个资源。它通过在服务端将资源（CSS文件，HTML片段，Javascript代码或者base64编码的图片）打包成一个由双方约定的字符串分割的长字符串，并发送到客户端。
   
   然后用Javascript代码处理这个长字符串，并根据他的mime-type类型和传入的其他‘头信息’解析出每个资源。
   
   例如，解析一串图片编码，输入为req.responseText
   
       function splitImages(imageString){
         var imageData = imageString.split('\u0001');
         var imageElement;

         for (var i =0, len = imageData.length; i<len; i++){
           imageElement = document.createElement('img');
           imageElement.src = 'data:image/jpeg;base64,' + imageData[i];
           document.getElementById('container').appendChild(imageElement);
         }
       }
