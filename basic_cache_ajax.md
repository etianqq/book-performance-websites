# 14.使Ajax可缓存

####Web2.0
Web2.0有两个重要特性：
* DHTML： 使用JS和CSS与DOM进行交互
* AJax：客户端可以获取和显示用户请求的新信息而无需重新加载页面

####Ajax
Ajax在UI和Web服务器之间插入了一层。这个Ajax层位于客户端，与Web服务器进行交互以获取请求的信息。

####缓存Ajax
有两个要求：
1. 必须是GET请求
2. 必须在response中发送正确的HTTP头信息：Expires

